package br.com.itau.pagamento.clients;

import br.com.itau.pagamento.models.Cartao;

public class CartaoClientFallback implements CartaoClient {
    @Override
    public Cartao consultarPorNumero(String numero) {
        Cartao cartao = new Cartao();

        cartao.setId(1);
        cartao.setCartaoNumero("007671928");
        cartao.setAtivo(true);

        return cartao;
    }
}
