package br.com.itau.pagamento.dtos;

public class PagamentoDTOEntradaPost {

    private String cartaoNumero;

    private String descricao;

    private double valor;

    public PagamentoDTOEntradaPost() { }

    public String getCartaoNumero() { return cartaoNumero; }

    public void setCartaoNumero(String cartaoNumero) { this.cartaoNumero = cartaoNumero; }

    public String getDescricao() { return descricao; }

    public void setDescricao(String descricao) { this.descricao = descricao; }

    public double getValor() { return valor; }

    public void setValor(double valor) { this.valor = valor; }
}
