package br.com.itau.pagamento.repositories;

import br.com.itau.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    Iterable<Pagamento> findAllByCartaoId(long cartaoId);

}
